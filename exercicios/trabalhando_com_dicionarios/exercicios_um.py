#tuplas
pessoa1 = ("Nico", 39)
pessoa2 = ("Flavio", 37)
pessoa3 = ("Marcos", 30)

# lista contendo tuplas
instrutores = [pessoa1, pessoa2, pessoa3]

#[('Nico', 39), ('Flavio', 37), ('Marcos', 30)]
print(instrutores)

#idade de flavio = 37
print(instrutores[1][1])

# Problemas
# E se não sabemos a posição do Flavio ? 
# Em geral, como podemos descobrir a idade de um instrutor sem saber a posição dele?
# Como descobrir a idade pelo nome - algo assim - instrutores['Flavio']
# o problema é que instrutores['Flavio'] não funciona nem com tuplas nem com listas
# para essa finalidade podemos utilizar dicionários

# parece um set mas não é - repare no :
instrutores_dict = {'Nico' : 39, 'Flavio': 37, 'Marcos' : 30}

print(instrutores_dict['Flavio']) # 37