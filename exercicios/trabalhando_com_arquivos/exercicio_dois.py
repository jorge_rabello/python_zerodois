# utilizando with garantimos que o python fechará o arquivo independente de erros
with open("exercicios/trabalhando_com_arquivos/pessoas.txt") as arquivo:
    for linha in arquivo:
        print(linha.strip())