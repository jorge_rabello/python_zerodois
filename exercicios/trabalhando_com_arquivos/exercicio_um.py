# modificadores de acesso
# r apenas leitura
# w escrita
# a append - para adicionar conteúdo a um arquivo existente
# r+ leitura e escrita

arquivo = open('exercicios/trabalhando_com_arquivos/pessoas.txt', 'r')
linha = arquivo.readline()
print(linha)