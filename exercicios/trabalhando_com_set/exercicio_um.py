colecao = {11122233344, 22233344455, 33344455566}

#vai adicionar pois não existe ainda
colecao.add(44455566677)

#nao vai adicionar pois este CPF já existe! - não interrompe a execução
colecao.add(11122233344) 

#nao funciona pois sets não possuem índices - erro de execução interrompe o programa
#colecao[0]

for cpf in colecao:
  print(cpf)