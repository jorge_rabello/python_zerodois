# tupla
valores = ("a","b","c","d","e")

# lista
letras = ["a","b","c","d","e"]

print(len(valores))
print(max(valores))
print(min(valores))

#del(valores)
#valores.pop()
#valores.append("f")

letras.pop()
print(letras)

letras.append("f")
print(letras)

del(letras)
print(letras)