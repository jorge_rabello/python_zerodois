import random

def jogar():
  banner()
  palavra_secreta = carrega_palavra_secreta()
  letras_encontradas = inicializa_letras_encontradas(palavra_secreta)
  calcula_letras_faltando(letras_encontradas)
  print(letras_encontradas)
  game_loop(palavra_secreta, letras_encontradas)
  print("Game Over")

def banner():
  print("********************************")
  print("**Bem vindo ao jogo de forca !**")
  print("********************************")

def carrega_palavra_secreta():
    with open('jogos/palavras.txt', 'r') as arquivo:
      palavras = []
      for linha in arquivo:
        palavras.append(linha.strip().upper())
      return palavras[random.randrange(0, len(palavras))]

def inicializa_letras_encontradas(palavra_secreta):
  return ["_" for letra in palavra_secreta]

def calcula_letras_faltando(letras_encontradas):
    letras_fantantes = str(letras_encontradas.count('_'))
    print("Ainda falta acertar {} letras".format(letras_fantantes))

def game_loop(palavra_secreta, letras_encontradas):
  enforcou = False
  acertou = False
  erros = 0

  while(not enforcou and not acertou):
    palpite = input("Digite uma letra para tentar adivinhar a palavra: ").strip().upper()
    if(palpite in palavra_secreta):
      marca_palpite_correto(palpite, palavra_secreta, letras_encontradas)
    else:
      erros += 1
      desenha_forca(erros)

    enforcou = erros == 7
    acertou = "_" not in letras_encontradas
    print(letras_encontradas)
    calcula_letras_faltando(letras_encontradas)

  if(acertou):
    imprime_mensagem_vencedor()
  else:
    imprime_mensagem_perdedor(palavra_secreta)

def marca_palpite_correto(palpite, palavra_secreta, letras_acertadas):
  posicao = 0
  for letra in palavra_secreta:
    if(palpite == letra):
      letras_acertadas[posicao] = letra
    posicao += 1

def imprime_mensagem_perdedor(palavra_secreta):
  print("Puxa, você foi enforcado!")
  print("A palavra era {}".format(palavra_secreta))
  print("    _______________         ")
  print("   /               \       ")
  print("  /                 \      ")
  print("//                   \/\  ")
  print("\|   XXXX     XXXX   | /   ")
  print(" |   XXXX     XXXX   |/     ")
  print(" |   XXX       XXX   |      ")
  print(" |                   |      ")
  print(" \__      XXX      __/     ")
  print("   |\     XXX     /|       ")
  print("   | |           | |        ")
  print("   | I I I I I I I |        ")
  print("   |  I I I I I I  |        ")
  print("   \_             _/       ")
  print("     \_         _/         ")
  print("       \_______/           ")

def imprime_mensagem_vencedor():
  print("Parabéns, você ganhou!")
  print("       ___________      ")
  print("      '._==_==_=_.'     ")
  print("      .-\\:      /-.    ")
  print("     | (|:.     |) |    ")
  print("      '-|:.     |-'     ")
  print("        \\::.    /      ")
  print("         '::. .'        ")
  print("           ) (          ")
  print("         _.' '._        ")
  print("        '-------'       ")

def desenha_forca(erros):
    print("  _______     ")
    print(" |/      |    ")

    if(erros == 1):
        print(" |      (_)   ")
        print(" |            ")
        print(" |            ")
        print(" |            ")

    if(erros == 2):
        print(" |      (_)   ")
        print(" |      \     ")
        print(" |            ")
        print(" |            ")

    if(erros == 3):
        print(" |      (_)   ")
        print(" |      \|    ")
        print(" |            ")
        print(" |            ")

    if(erros == 4):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |            ")
        print(" |            ")

    if(erros == 5):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |       |    ")
        print(" |            ")

    if(erros == 6):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |       |    ")
        print(" |      /     ")

    if (erros == 7):
        print(" |      (_)   ")
        print(" |      \|/   ")
        print(" |       |    ")
        print(" |      / \   ")

    print(" |            ")
    print("_|___         ")
    print()

if(__name__ == "__main__"):
  jogar()